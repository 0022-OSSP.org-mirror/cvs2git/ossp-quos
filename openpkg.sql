--
--  OSSP quos - Query On Steroids
--  Copyright (c) 2004 Ralf S. Engelschall <rse@engelschall.com>
--  Copyright (c) 2004 The OSSP Project <http://www.ossp.org/>
--
--  This file is part of OSSP quos, a Web user interface for querying
--  a database which can be found at http://www.ossp.org/pkg/tool/quos/.
--
--  This program is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 2 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--  General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program; if not, write to the Free Software
--  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
--  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
--
--  quos-openpkg.sql: OSSP quos database schema for OpenPKG XML/RDF
--

CREATE TABLE quos_rdf (
    rd_id           INTEGER UNIQUE NOT NULL PRIMARY KEY,
    rd_url          TEXT
);

CREATE TABLE quos_package (
    pk_id           INTEGER UNIQUE NOT NULL PRIMARY KEY,
    pk_name         TEXT,
    pk_version      TEXT,
    pk_release      TEXT,
    pk_distribution TEXT,
    pk_group        TEXT,
    pk_license      TEXT,
    pk_packager     TEXT,
    pk_summary      TEXT,
    pk_url          TEXT,
    pk_vendor       TEXT,
    pk_description  TEXT,
    pk_rdf          INTEGER NOT NULL
                    REFERENCES quos_rdf (rd_id)
                    MATCH FULL DEFERRABLE
);

CREATE TABLE quos_buildprereq (
    bp_id           INTEGER NOT NULL
                    REFERENCES quos_package (pk_id)
                    MATCH FULL DEFERRABLE,
    bp_key          TEXT,         
    bp_op           TEXT,
    bp_val          TEXT
);

CREATE TABLE quos_prereq (
    rp_id           INTEGER NOT NULL
                    REFERENCES quos_package (pk_id)
                    MATCH FULL DEFERRABLE,
    rp_key          TEXT,         
    rp_op           TEXT,
    rp_val          TEXT
);

CREATE TABLE quos_provide (
    pr_id           INTEGER NOT NULL
                    REFERENCES quos_package (pk_id)
                    MATCH FULL DEFERRABLE,
    pr_key          TEXT,         
    pr_op           TEXT,
    pr_val          TEXT
);

CREATE TABLE quos_source (
    sr_id           INTEGER NOT NULL
                    REFERENCES quos_package (p_id)
                    MATCH FULL DEFERRABLE,
    sr_url          TEXT
);

