#!/usr/opkg/bin/perl -w
##
##  OSSP quos - Query On Steroids
##  Copyright (c) 2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP quos, a Web user interface for querying
##  a database which can be found at http://www.ossp.org/pkg/tool/quos/.
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 2 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  quos.cgi: web user interface
##

require 5.008;
use strict;
use CGI;
use String::Divert;
use IO::File;
use Data::Dumper;

#   internal handling of fatal errors
BEGIN {
    $SIG{__DIE__} = sub {
        my ($msg) = @_;
        my $hint = '';
        if ($msg =~ m|line\s+(\d+)|) {
            my $line = $1;
            my $io = new IO::File "<$0";
            my @code = $io->getlines();
            $io->close();
            my $i = -1;
            $hint = join("", map { s/^/sprintf("%d: ", $line+$i++)/se; $_; } @code[$line-2..$line]);
        }
        print STDOUT 
            "Content-Type: text/html; charset=ISO-8859-1\n" .
            "\n" .
            "<html>\n" .
            "  <head>\n" .
            "    <title>OSSP quos: GURU MEDITATION</title>\n" .
            "    <style type=\"text/css\">\n" .
            "      HTML {\n" .
            "          width:  100%;\n" .
            "          height: auto;\n" .
            "      }\n" .
            "      BODY {\n" .
            "          background: #cccccc;\n" .
            "          margin:     0 0 0 0;\n" .
            "          padding:    0 0 0 0;\n" .
            "      }\n" .
            "      DIV.canvas {\n" .
            "          background: #000000;\n" .
            "          border: 20px solid #000000;\n" .
            "      }\n" .
            "      DIV.error1 {\n" .
            "          border-top:          4px solid #cc3333;\n" .
            "          border-left:         4px solid #cc3333;\n" .
            "          border-right:        4px solid #cc3333;\n" .
            "          border-bottom:       4px solid #cc3333;\n" .
            "          padding:             10px 10px 10px 10px;\n" .
            "          font-family:         sans-serif, helvetica, arial;\n" .
            "          background:          #000000;\n" .
            "          color:               #cc3333;\n" .
            "      }\n" .
            "      DIV.error2 {\n" .
            "          border-top:          4px solid #000000;\n" .
            "          border-left:         4px solid #000000;\n" .
            "          border-right:        4px solid #000000;\n" .
            "          border-bottom:       4px solid #000000;\n" .
            "          padding:             10px 10px 10px 10px;\n" .
            "          font-family:         sans-serif, helvetica, arial;\n" .
            "          background:          #000000;\n" .
            "          color:               #cc3333;\n" .
            "      }\n" .
            "      SPAN.title {\n" .
            "          font-size:           200%;\n" .
            "          font-weight:         bold;\n" .
            "      }\n" .
            "      TT.text {\n" .
            "          font-weight:         bold;\n" .
            "      }\n" .
            "    </style>\n" .
            "    <script language=\"JavaScript\">\n" .
            "    var count = 0;\n" .
            "    function blinker() {\n" .
            "        var obj = document.getElementById('error');\n" .
            "        if (count++ % 2 == 0) {\n" .
            "            obj.className = 'error1';\n" .
            "        }\n" .
            "        else {\n" .
            "            obj.className = 'error2';\n" .
            "        }\n" .
            "        setTimeout('blinker()', 1000);\n" .
            "    }\n" .
            "    </script>\n" .
            "  </head>\n" .
            "  <body onLoad=\"setTimeout('blinker()', 1);\">\n" .
            "    <div class=\"canvas\">\n" .
            "      <div id=\"error\" class=\"error1\">\n" .
            "        <span class=\"title\">OSSP quos: GURU MEDITATION</span>\n" .
            "        <p>\n" .
            "        <tt class=\"text\">\n" .
            "          $msg<br>\n" .
            "        </tt>\n" .
            "        <pre>\n$hint</pre>\n" .
            "      </div>\n" .
            "    </div>\n" .
            "  </body>\n" .
            "</html>\n";
        exit(0);
    };
}

#   create objects
my $cgi  = new CGI;
my $html = new String::Divert;
$html->overload(1);

##
##  Read Config File
##

my $cfg = {
    -cols => 4,
    -rows => 7,
};
#   FIXME

##
##  Query Processing
##

my $nr_cols = ($cgi->param('nr_cols') || $cfg->{-cols});
my $nr_rows = ($cgi->param('nr_rows') || $cfg->{-rows});

my $err = {
    -msg  => '',
    -form => {},
};

&validate_form($cgi, $err, $nr_cols, $nr_rows);
my $expr = &parse_form($cgi, $err, undef, $nr_cols-1, 0, 0, $nr_rows-1, 0);

sub validate_form {
    my ($cgi, $err, $nr_cols, $nr_rows) = @_;
    for (my $y = 1; $y <= $nr_rows-1; $y++) {
        my $n = 0;
        my $fn = $cgi->param(sprintf("fn%02d", $y));
        my $fo = $cgi->param(sprintf("fo%02d", $y));
        if (defined($fn) and defined($fo)) {
            if (($fn ne '' and $fo eq '') or ($fn eq '' and $fo ne '')) {
                $err->{-msg} .= "form line ".($y+1).": both field name and operator required\n";
                $err->{-form}->{sprintf("fn%02d", $y)}++;
                $err->{-form}->{sprintf("fo%02d", $y)}++;
                $err->{-form}->{sprintf("fv%02d", $y)}++;
            }
            if ($fn ne '' or $fo ne '') {
                for (my $x = $nr_cols-1; $x >= 0; $x--) {
                    my $op = $cgi->param(sprintf("lo%02d%02d", $x, $y));
                    if (defined($op) and $op ne ' ') {
                        $n++;
                        last;
                    }
                }
                if ($n == 0) {
                    $err->{-msg} .= "form line ".($y+1).": no operator for sub-expression found\n";
                    $err->{-form}->{sprintf("fn%02d", $y)}++;
                    $err->{-form}->{sprintf("fo%02d", $y)}++;
                    $err->{-form}->{sprintf("fv%02d", $y)}++;
                }
            }
        }
    }
}

sub parse_form {
    my ($cgi, $err, $erase, $tlx, $tly, $brx, $bry, $depth) = @_;
    $erase = {} if (not defined($erase));
    my $expr = undef;
    if ($tlx < 0) {
        #   field operator area
        my $fn = $cgi->param(sprintf("fn%02d", $tly));
        my $fo = $cgi->param(sprintf("fo%02d", $tly));
        my $fv = $cgi->param(sprintf("fv%02d", $tly));
        if (defined($fn) and defined($fo) and $fn ne '' and $fo ne '') {
            $expr = [ $fo, $fn, $fv ];
        }
    }
    else {
        #   logical operator area
        LOOP: for (my $x = $tlx; $x >= $brx; $x--) {
            for (my $y = $tly; $y <= $bry; $y++) {
                my $op = $cgi->param(sprintf("lo%02d%02d", $x, $y));
                if (defined($op) and $op ne ' ' and not $erase->{$x."/".$y}) {
                    $erase->{$x."/".$y} = 1;
                    if ($op eq '!') {
                        my $expr1 = &parse_form($cgi, $erase, $x, $y, $brx, $bry, $depth+1);
                        $expr = [ $op, $expr1 ];
                    }
                    else {
                        if ($tly > $y-1) {
                            $err->{-msg} .= "form line ".($y+1).", column ".($nr_cols-$x).": unexpected binary operator\n";
                            $err->{-form}->{sprintf("lo%02d%02d", $x, $y)}++;
                        }
                        my $expr1 = &parse_form($cgi, $err, $erase, $x-1, $tly, $brx, $y-1, $depth+1);
                        my $expr2 = &parse_form($cgi, $err, $erase, $x, $y, $brx, $bry, $depth+1);
                        if (not defined($expr1)) {
                            $err->{-msg} .= "form line ".($y+1).", column ".($nr_cols-$x).": no expressions LHS found\n";
                            $err->{-form}->{sprintf("lo%02d%02d", $x, $y)}++;
                        }
                        if (not defined($expr2)) {
                            $err->{-msg} .= "form line ".($y+1).", column ".($nr_cols-$x).": no expressions RHS found\n";
                            $err->{-form}->{sprintf("lo%02d%02d", $x, $y)}++;
                        }
                        $expr = [ $op, $expr1, $expr2 ];
                    }
                    last LOOP;
                }
            }
        }
        if (not defined($expr)) {
            $expr = &parse_form($cgi, $err, $erase, $brx-1, $tly, $brx-1, $tly, $depth+1);
        }
    }
    return $expr;
}

my $cstyle = &expr2cstyle($expr);
sub expr2cstyle {
    my ($expr) = @_;
    my $op    = $expr->[0];
    my $cstyle;
    if ($op eq '!') {
        my $expr1 = ref($expr->[1]) ? &expr2cstyle($expr->[1]) : $expr->[1];
        $cstyle = sprintf("(%s %s)", $op, $expr1);
    }
    else {
        my $expr1 = ref($expr->[1]) ? &expr2cstyle($expr->[1]) : $expr->[1];
        my $expr2 = ref($expr->[2]) ? &expr2cstyle($expr->[2]) : $expr->[2];
        $cstyle = sprintf("(%s %s %s)", $expr1, $op, $expr2);
    }
    return $cstyle;
}

##
##  Generate Canvas
##

#   generate Javascript utility functions
$html .= "<script language=\"JavaScript\">\n" .
         "<!--\n" .
         "function colorize(obj, def) {\n" .
         "    if (obj.value == def) { obj.className = 'default'; }\n" .
         "    else                  { obj.className = 'changed'; }\n" .
         "    return true;\n" .
         "}\n" .
         "//-->\n" .
         "</script>\n";

#   generate outer canvas
$html .= "<table class=\"quos\">\n";
$html .= "  "; $html->fold("quos");
$html .= "</table>\n";
$html >> "quos";

#   generate browse part   
$html .= "<tr>\n";
$html .= "  <td class=\"browse\">\n";
$html .= "    <span class=\"title\">Browse</span><br>\n";
$html .= "    "; $html->fold("browse");
$html .= "  </td>\n";
$html .= "</tr>\n";

#   generate query part   
$html .= "<tr>\n";
$html .= "  <td class=\"query\">\n";
$html .= "    <span class=\"title\">Query</span><br>\n";
$html .= "    "; $html->fold("query");
$html .= "  </td>\n";
$html .= "</tr>\n";

#   generate view part   
$html .= "<tr>\n";
$html .= "  <td class=\"view\">\n";
$html .= "    <span class=\"title\">View</span><br>\n";
$html .= "    "; $html->fold("view");
$html .= "  </td>\n";
$html .= "</tr>\n";

#   generate result part   
$html .= "<tr>\n";
$html .= "  <td class=\"result\">\n";
$html .= "    <span class=\"title\">Result</span><br>\n";
$html .= "    "; $html->fold("result");
$html .= "  </td>\n";
$html .= "</tr>\n";

##
##  Generate Browser Part
##

$html >> "browse";
$html .= "FIXME";
$html << 1;

##
##  Generate Query Part
##

$html >> "query";

#   render outer form canvas
$html .= $cgi->start_form;
$html .= "<table>\n";
$html .= "  <tr>\n";
$html .= "    <td class=\"expr\">\n";
$html .= "      "; $html->fold("query-form-expr");
$html .= "    </td>\n";
$html .= "  </tr>\n";
$html .= "  <tr>\n";
$html .= "    <td class=\"button\">\n";
$html .= "      "; $html->fold("query-form-button");
$html .= "    </td>\n";
$html .= "  </tr>\n";
$html .= "  <tr>\n";
$html .= "    <td class=\"define\">\n";
$html .= "      "; $html->fold("query-form-define");
$html .= "    </td>\n";
$html .= "  </tr>\n";
$html .= "  <tr>\n";
$html .= "    <td class=\"status\">\n";
$html .= "      "; $html->fold("query-form-status");
$html .= "    </td>\n";
$html .= "  </tr>\n";
$html .= "</table>\n";
$html .= $cgi->end_form;

#   render expression part
$html >> "query-form-expr";
$html .= "<table>\n";
for (my $i = 0; $i < $nr_rows; $i++) {
    $html .= "<tr>\n";
    for (my $j = $nr_cols - 1; $j >= 0; $j--) {
        $html .= "  <td>\n";
        my $id = sprintf("lo%02d%02d", $j, $i);
        $html .= "    " . $cgi->popup_menu(
            -id       => $id,
            -name     => $id,
            -values   => [' ', '!', '&', '|'],
            -default  => ' ',
            -class    => ($err->{-form}->{$id} ? 'error' : 'default'),
            -onChange => "return colorize(this.form.$id, ' ');",
            -onBlur   => "return colorize(this.form.$id, ' ');",
        ) . "\n";
        $html .= "  </td>\n";
    }
    $html .= "  <td>\n";
    my $id = sprintf("fn%02d", $i);
    $html .= "    " . $cgi->popup_menu(
        -id       => $id,
        -name     => $id,
        -values   => ['', 'name', 'description', 'foo'], # FIXME
        -default  => '', # FIXME
        -class    => ($err->{-form}->{$id} ? 'error' : 'default'),
        -onChange => "return colorize(this.form.$id, '');",
        -onBlur   => "return colorize(this.form.$id, '');",
    ) . "\n";
    $html .= "  </td>\n";
    $html .= "  <td>\n";
    $id = sprintf("fo%02d", $i);
    $html .= "    " . $cgi->popup_menu(
        -id       => $id,
        -name     => $id,
        -values   => ['', '==', '!=', '<', '<=', '>', '>=', '=~', '!~'],
        -default  => '',
        -class    => ($err->{-form}->{$id} ? 'error' : 'default'),
        -onChange => "return colorize(this.form.$id, '');",
        -onBlur   => "return colorize(this.form.$id, '');",
    ). "\n";
    $html .= "  </td>\n";
    $html .= "  <td>\n";
    $id = sprintf("fv%02d", $i);
    $html .= "    " . $cgi->textfield(
        -id         => $id,
        -name       => $id,
        -default    => '',
        -class    => ($err->{-form}->{$id} ? 'error' : 'default'),
        -onChange   => "return colorize(this.form.$id, '');",
        -onBlur     => "return colorize(this.form.$id, '');",
        -onMouseOut => "return colorize(this.form.$id, '');",
    );
    $html .= "  </td>\n";
    $html .= "</tr>\n";
}
$html .= "</table>\n";
$html << 1;

#   render button part
$html >> "query-form-button";
$html .= "<table>\n";
$html .= "  <tr>\n";
$html .= "    <td>";
$html .= "      " . "&larr; " . $cgi->popup_menu(
    -name    => "nr_cols",
    -values  => [ 0..9 ],
    -default => $nr_cols,
) . "\n";
$html .= "      " . "&darr; " . $cgi->popup_menu(
    -name    => "nr_rows",
    -values  => [1..9],
    -default => $nr_rows,
) . "\n";
$html .= "    </td>\n";
$html .= "    <td>\n";
$html .= "      " . $cgi->submit(
    -name  => 'act_matrix_upd',
    -value => 'update',
    -class => 'update'
) . "\n";
$html .= "    </td>\n";
$html .= "    <td>\n";
$html .= "      " . $cgi->submit(
    -name  => 'act_matrix_clear',
    -value => 'clear',
    -class => 'clear'
) . "\n";
$html .= "    </td>\n";
$html .= "    <td>\n";
$html .= "      " . $cgi->submit(
    -name  => 'act_matrix_submit',
    -value => 'submit',
    -class => 'submit'
) . "\n";
$html .= "    </td>\n";
$html .= "  </tr>\n";
$html .= "</table>\n";
$html << 1;

#   render define part
$html >> "query-form-define";
$html .= "<table>\n";
$html .= "  <tr>\n";
$html .= "    <td>\n";
$html .= "    " . $cgi->popup_menu(
    -id       => "namexx",
    -name     => "namexx",
    -values   => ['HTML: by-group', 'HTML: by-xxx', 'TXT: by-group'], # FIXME
    -default  => 'HTML: by-group', # FIXME
    -class    => 'nameget',
) . "\n";
$html .= "    </td>\n";
$html .= "    <td>\n";
$html .= "      " . $cgi->submit(
    -name  => 'act_matrix_clear',
    -value => 'load',
    -class => 'load'
) . "\n";
$html .= "    </td>\n";
$html .= "    <td>\n";
$html .= "      " . $cgi->submit(
    -name  => 'act_matrix_submit',
    -value => 'delete',
    -class => 'delete'
) . "\n";
$html .= "    </td>\n";
$html .= "  </tr>\n";
$html .= "  <tr>\n";
$html .= "    <td>\n";
$html .= "      " . $cgi->textfield(
    -name  => 'name_persquery',
    -value => '',
    -class => 'nameset'
) . "\n";
$html .= "    </td>\n";
$html .= "    <td>\n";
$html .= "      " . $cgi->submit(
    -name  => 'act_matrix_clear',
    -value => 'save',
    -class => 'save'
) . "\n";
$html .= "    </td>\n";
$html .= "    <td>\n";
$html .= "      " . $cgi->submit(
    -name  => 'act_matrix_hyperlink',
    -value => 'hyperlink',
    -class => 'hyperlink'
) . "\n";
$html .= "    </td>\n";
$html .= "  </tr>\n";
$html .= "</table>\n";
$html << 1;

#   render status part
if ($err->{-msg} ne '') {
    $html >> "query-form-status";
    my $msg = $err->{-msg};
    $msg =~ s/\n/<br>/sg;
    $html .= "<span class=\"msg\">$msg</span>\n";
    $html << 1;
}

$html << 1;

##
##  Generate View Part
##

$html >> "view";
$html .= "FIXME";
$html << 1;

##
##  Generate Result Part
##

$html >> "result";
#foreach my $key ($cgi->param) {
#    $html .= "<strong>$key</strong> -&gt; ";
#    my @values = $cgi->param($key);
#    $html .= join(", ", @values) . "<br>\n";
#}
#$html << 1;
$html .= "\$expr:\n" . Data::Dumper->Dump([$expr]). "<p>";
$html .= "\$cstyle:\n". $cstyle . "<br>";

##
##  Generate HTTP Reponse
##

#   read HTML skeleton
my $io = new IO::File "<quos.html"
    or die "unable to read HTML skeleton file \"quos.html\"";
my $skel = '';
$skel .= $_ while (<$io>);
$io->close();

#   undivert HTML output and wrap with skeleton
$html->undivert(0);
$skel =~ s|\%BODY\%|$html|s;
$html = $skel;

#   create HTTP response
my $http = $cgi->header(
    -type    => 'text/html',
    -expires => '+10s'
);
$http .= $html;
print STDOUT $http;

#   die gracefully ;-)
exit(0);

__END__

=pod

=head1 NAME

B<OSSP quos> - Query On Steroids

=head1 DESCRIPTION

...FIXME...

=head1 LOGICAL LAYOUT

1. Browse
   - Groups
     . Hyperlinks
2. Query
   - Groups
     . Field (database schema)
     . Operator (compare)
     . Value (depends on compare)
3. View
   - Selection predefined output format
   - OSSP var based formatting string
   - ordering
   - save custom view
4. Result
   - based on View

=cut

